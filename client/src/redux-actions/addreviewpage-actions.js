import fetch from 'isomorphic-fetch'

export const ADD_REVIEW_EVENT = 'ADD_REVIEW_EVENT'
export const addReviewEvent = (postData) => {
  console.log(JSON.stringify(postData))
  return (dispatch, getState) => {
    const fetchOptions = {
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify(postData)
    }
    fetch('/api/Reviews', fetchOptions)
    .then((response) => {
      if(response.status < 200 || response.status > 299 ) {
        throw new Error("Bad response from server")
      }
      return response.json()
    })
    .then((json) => {
      store.dispatch(addReviewActionSuccess(json))
    })
    .catch((err)=> {
      store.dispatch(addReviewActionFailed(err))
    })
  }
}

export const ADD_REVIEW_ACTION_SUCCESS = 'ADD_REVIEW_ACTION_SUCCESS'
export const addReviewActionSuccess = (result) => {
  return {
    type: ADD_REVIEW_ACTION_SUCCESS,
    result
  }
}

export const ADD_REVIEW_ACTION_FAILED = 'ADD_REVIEW_ACTION_FAILED'
export const addReviewActionFailed = (err) => {
  return {
    type: ADD_REVIEW_ACTION_FAILED,
    err
  }
}

export const GET_PRODUCTLIST_EVENT = 'GET_PRODUCTLIST_EVENT'
export const getProductListEvent = () => {
  return (dispatch, getState) => {
    const fetchOptions = {
      method: 'GET',
      headers: {
        'Accept': 'application/json'
      }
    }
    fetch('/api/Products', fetchOptions)
    .then((response) => {
      if(response.status < 200 || response.status > 299) {
        throw new Error("Bad response from server");
      }
      return response.json()
    })
    .then((json) => {
      store.dispatch(getProductListActionSuccess(json))
    })
    .catch((err)=>{
      store.dispatch(getProductListActionFailed(err))
    })
  }
}

export const GET_PRODUCTLIST_ACTION_SUCCESS = 'GET_PRODUCTLIST_ACTION_SUCCESS'
export const getProductListActionSuccess = (result) => {
  return {
    type: GET_PRODUCTLIST_ACTION_SUCCESS,
    result
  }
}

export const GET_PRODUCTLIST_ACTION_FAILED = 'GET_PRODUCTLIST_ACTION_FAILED'
export const getProductListActionFailed = (err) => {
  return {
    type: GET_PRODUCTLIST_ACTION_FAILED,
    err
  }
}

export const SET_SELECTED_PRODUCT_ACTION = 'SET_SELECTED_PRODUCT_ACTION'
export const setSelectedProductAction = (productOption) => {
  return {
    type: SET_SELECTED_PRODUCT_ACTION,
    productOption
  }
}

export const SET_SELECTED_RATING_ACTION = 'SET_SELECTED_RATING_ACTION'
export const setSelectedRatingAction = (ratingOption) => {
  return {
    type: SET_SELECTED_RATING_ACTION,
    ratingOption
  }
}

export const SET_EMAIL_INPUT_ACTION = 'SET_EMAIL_INPUT_ACTION'
export const setEmailInputAction = (value) => {
  return {
    type: SET_EMAIL_INPUT_ACTION,
    value
  }
}

export const SET_REVIEW_COMMENT_ACTION = 'SET_REVIEW_COMMENT_ACTION'
export const setReviewCommentAction = (value) => {
  return {
    type: SET_REVIEW_COMMENT_ACTION,
    value
  }
}

export const SET_BAD_RATING_OPTION_ACTION = 'SET_BAD_RATING_OPTION_ACTION'
export const setBadRatingOptionAction = () => {
  return {
    type: SET_BAD_RATING_OPTION_ACTION
  }
}

export const SET_BAD_PRODUCT_OPTION_ACTION = 'SET_BAD_PRODUCT_OPTION_ACTION'
export const setBadProductOptionAction = () => {
  return {
    type: SET_BAD_PRODUCT_OPTION_ACTION
  }
}

export const SET_BAD_EMAIL_INPUT_ACTION = 'SET_BAD_EMAIL_INPUT_ACTION'
export const setBadEmailInputAction = () => {
  return {
    type: SET_BAD_EMAIL_INPUT_ACTION
  }
}

export const SET_BAD_REVIEW_COMMENT_ACTION = 'SET_BAD_REVIEW_COMMENT_ACTION'
export const setBadReviewCommentAction = () => {
  return {
    type: SET_BAD_REVIEW_COMMENT_ACTION
  }
}

export const RESET_REVIEW_REDUCER_STATE_ACTION = 'RESET_REVIEW_REDUCER_STATE_ACTION'
export const resetReviewReducerStateAction = () => {
  return {
    type: RESET_REVIEW_REDUCER_STATE_ACTION
  }
}

export const GET_USERPROFILELIST_EVENT = 'GET_USERPROFILELIST_EVENT'
export const getUserProfileListEvent = () => {
  return (dispatch, getState) => {
    const fetchOptions = {
      method: 'GET',
      headers: {
        'Accept': 'application/json'
      }
    }
    fetch('/api/UserProfiles', fetchOptions)
    .then((response) => {
      if(response.status < 200 || response.status > 299) {
        throw new Error("Bad response from server");
      }
      return response.json()
    })
    .then((json) => {
      store.dispatch(getUserProfileListActionSuccess(json))
    })
    .catch((err)=>{
      store.dispatch(getUserProfileListActionFailed(err))
    })
  }
}

export const GET_USERPROFILELIST_ACTION_SUCCESS = 'GET_USERPROFILELIST_ACTION_SUCCESS'
export const getUserProfileListActionSuccess = (result) => {
  return {
    type: GET_USERPROFILELIST_ACTION_SUCCESS,
    result
  }
}

export const GET_USERPROFILELIST_ACTION_FAILED = 'GET_USERPROFILELIST_ACTION_FAILED'
export const getUserProfileListActionFailed = (err) => {
  return {
    type: GET_USERPROFILELIST_ACTION_FAILED,
    err
  }
}
