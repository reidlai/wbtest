import fetch from 'isomorphic-fetch'

export const GET_LATESTPRODUCTS_EVENT = 'GET_LATESTPRODUCTS_EVENT'
export const getLatestProductsEvent = (brandId) => {
  return (dispatch, getState) => {
    let url = '/api/Products/getLatestProducts'
    if(brandId) {
      url = url + "?brandId=" + brandId
    }
    const fetchOptions = {
      method: 'GET',
      headers: {
        'Accept': 'application/json'
      }
    }
    fetch(url, fetchOptions)
    .then((response) => {
      if(response.status < 200 || response.status > 299) {
        throw new Error("Bad response from server");
      }
      return response.json()
    })
    .then((json) => {
      store.dispatch(getLatestProductsActionSuccess(json.data))
    })
    .catch((err)=>{
      store.dispatch(getLatestProductsActionFailed(err))
    })
  }
}

export const GET_LATESTPRODUCTS_ACTION_SUCCESS =
'GET_LATESTPRODUCTS_ACTION_SUCCESS'
export const getLatestProductsActionSuccess = (result) => {
  return {
    type: GET_LATESTPRODUCTS_ACTION_SUCCESS,
    result
  }
}

export const GET_LATESTPRODUCTS_ACTION_FAILED =
'GET_LATESTPRODUCTS_ACTION_FAILED'
export const getLatestProductsActionFailed = (err) => {
  return {
    type: GET_LATESTPRODUCTS_ACTION_FAILED,
    err
  }
}

export const GET_BRANDLIST_EVENT = 'GET_BRANDLIST_EVENT'
export const getBrandListEvent = () => {
  return (dispatch, getState) => {
    const fetchOptions = {
      method: 'GET',
      headers: {
        'Accept': 'application/json'
      }
    }
    fetch('/api/Brands', fetchOptions)
    .then((response) => {
      if(response.status < 200 || response.status > 299) {
        throw new Error("Bad response from server");
      }
      return response.json()
    })
    .then((json) => {
      store.dispatch(getBrandListActionSuccess(json))
    })
    .catch((err)=>{
      store.dispatch(getBrandListActionFailed(err))
    })
  }
}

export const GET_BRANDLIST_ACTION_SUCCESS = 'GET_BRANDLIST_ACTION_SUCCESS'
export const getBrandListActionSuccess = (result) => {
  return {
    type: GET_BRANDLIST_ACTION_SUCCESS,
    result
  }
}

export const GET_BRANDLIST_ACTION_FAILED = 'GET_BRANDLIST_ACTION_FAILED'
export const getBrandListActionFailed = (err) => {
  return {
    type: GET_BRANDLIST_ACTION_FAILED,
    err
  }
}

export const FILTER_BY_BRAND_EVENT = 'FILTER_BY_BRAND_EVENT'
export const filterByBrandEvent = (brandOption) => {
  return (dispatch, getState) => {
    dispatch(setBrandFilterAction(brandOption))
    dispatch(getLatestProductsEvent(brandOption.value))
  }
}

export const SET_BRAND_FILTER_ACTION = 'SET_BRAND_FILTER_ACTION'
export const setBrandFilterAction = (brandOption) => {
  return {
    type: SET_BRAND_FILTER_ACTION,
    brandOption
  }
}
