import _ from 'lodash'
import * as productPageActions from './productpage-actions'
import * as addReviewPageActions from './addreviewpage-actions'

const actions =  _.assign(
  {},
  productPageActions,
  addReviewPageActions
)

export default actions
