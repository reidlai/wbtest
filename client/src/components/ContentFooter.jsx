import React, { Component } from 'react'
import Footer from 'grommet/components/Footer'
import Box from 'grommet/components/Box'
import Paragraph from 'grommet/components/Paragraph'

class ContentFooter extends Component {
  render() {
    return(
      <Footer justify='between'>
        <Box direction='row'
          align='center'
          pad={{"between": "medium"}}>
          <Paragraph margin='none'>
            © 2017 Reid Lai
          </Paragraph>
        </Box>
      </Footer>
    )
  }
}

export default ContentFooter
