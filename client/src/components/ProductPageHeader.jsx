import React, { Component } from 'react'
import {connect} from 'react-redux'
import _ from 'lodash'

import Header from 'grommet/components/Header'
import Title from 'grommet/components/Title'
import Box from 'grommet/components/Box'
import Search from 'grommet/components/Search'
import Select from 'grommet/components/Select'
import Menu from 'grommet/components/Menu'
import More from 'grommet/components/icons/base/More'
import Anchor from 'grommet/components/Anchor'
import Label from 'grommet/components/Label'

import reduxActions from '../redux-actions'

class ProductPageHeader extends Component {
  constructor(props) {
    super(props)
    this.state = props
    this.onSelectChanged = this.onSelectChanged.bind(this)
  }

  handleStateChange() {
    let reduxState = store.getState()
    this.setState(_.defaultsDeep(mapStateToProps(reduxState), this.state))
  }

  componentDidMount() {
    this.unsubscribe = store.subscribe(this.handleStateChange.bind(this))
  }

  componentWillUnmount() {
    this.unsubscribe()
  }

  render() {
    let selectData = _.map(this.state.brandData, (item) => {
      return {label: item.brandName, value: item.brandId}
    })
    selectData = _.union([{label: 'All', value: null}], selectData)
    return (
      <Header>
        <Title>
          Latest 10 Products
        </Title>
        <Box flex={true} justify='end' direction='row' responsive={false}>
          <Select value={this.state.filteredBrand} placeHolder='Brand'
          options={selectData} onChange={this.onSelectChanged} />
        </Box>
      </Header>
    )
  }

  onSelectChanged(options) {
    dispatch(reduxActions.filterByBrandEvent(options.option))
    dispatch(reduxActions.getLatestProductsEvent(options.option.value))
  }
}

const mapStateToProps = (state) => {
  return {
    ...state.brandReducer,
    ...state.productReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {dispatch}
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductPageHeader)
