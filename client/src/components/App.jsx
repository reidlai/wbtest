import React, { Component } from 'react'
import { BrowserRouter, Route, Link, Switch } from 'react-router-dom'

import GrommetApp from 'grommet/components/App'
import Split from 'grommet/components/Split'
import Sidebar from 'grommet/components/Sidebar'
import Header from 'grommet/components/Header'
import Title from 'grommet/components/Title'
import Box from 'grommet/components/Box'
import Menu from 'grommet/components/Menu'
import Anchor from 'grommet/components/Anchor'
import Responsive from 'grommet/utils/Responsive'

import ProductPage from './ProductPage.jsx'
import AddReviewPage from './AddReviewPage.jsx'
import ContentFooter from './ContentFooter.jsx'

export class App extends Component {

  constructor(props) {
    super(props)
    this.state = props
  }

  render() {
    return (
      <BrowserRouter>
        <GrommetApp centered={false}>
          <Split flex="right">
            <Sidebar colorIndex='neutral-1' fixed={false} size='medium'>
              <Header pad='medium' justify='between'>
                <Title>
                  Menu
                </Title>
              </Header>
              <Box flex='grow' justify='start'>
                <Menu primary={true}>
                  <Anchor path='/' className='active'>
                    Product List
                  </Anchor>
                  <Anchor path='/addReview'>
                    AddReview
                  </Anchor>
                </Menu>
              </Box>
            </Sidebar>
            <Box colorIndex='light-2' pad={{horizontal: 'medium', vertical: 'none'}} full={true}>
              <Switch>
                <Route exact path="/addReview/:productId?" component={AddReviewPage} />
                <Route exact path="/" component={ProductPage} />
              </Switch>
              <ContentFooter />
            </Box>
          </Split>
        </GrommetApp>
      </BrowserRouter>
    )
  }
}

export default App
