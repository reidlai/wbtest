import React, { Component } from 'react'
import Card from 'grommet/components/Card'
import Timestamp from 'grommet/components/Timestamp'
import Box from 'grommet/components/Box'
import Button from 'grommet/components/Button'
import Edit from 'grommet/components/icons/base/Edit'
import Header from 'grommet/components/Header'
import Footer from 'grommet/components/Footer'
import Paragraph from 'grommet/components/Paragraph'
import { Redirect } from 'react-router-dom'
import {$set} from 'plow-js'
import moment from 'moment'

import reduxActions from '../redux-actions'

class ProductCard extends Component {
  constructor(props) {
    super(props)
    this.state = $set('redirectToAddReview', false, props)
    this.onAddReviewButtonClicked = this.onAddReviewButtonClicked.bind(this)
  }

  render() {
    const product = this.state.product
    const redirectUrl = `/addReview/${this.state.product.productId}`
    if(this.state.redirectToAddReview) {
      return (<Redirect push to={redirectUrl} />)
    }

    let comment = null
    if(product.reviewId) {
      comment = (
        <div>
          <Box flex={true} direction='column'>
            <Paragraph margin='none' align='start' size='large'>
              {product.userName}
            </Paragraph>
            <Paragraph margin='none' size='medium'>
              {product.reviewComment}
            </Paragraph>
            <Paragraph margin='none' align='end' size='small'>
              <Timestamp value={moment(product.reviewCreationTimestamp).toDate()} align='end'
              fields='date' />
            </Paragraph>
          </Box>
          <br />
          <div style={{height: '1px', width: '100%', backgroundColor: '#CCCCCC'}} />
        </div>
      )
    }
    return(
      <Card
      colorIndex='light-1' contentPad='small'
      heading={product.productName}
      label={product.brandName}
      description={product.productDescription}>
        <Timestamp value={moment(product.productCreationTimestamp).toDate()} align='end'
        fields='date' />
        <br />
        <div style={{height: '1px', width: '100%', backgroundColor: '#CCCCCC'}} />
        {comment}
        <Box>
          <Button icon={<Edit />} label='Add Review' primary={false}
          fill={false} plain={true} onClick={this.onAddReviewButtonClicked} />
        </Box>
      </Card>
    )
  }

  onAddReviewButtonClicked() {
    store.dispatch(reduxActions.setSelectedProductAction({
      value: this.state.product.productId, label: this.state.product.productName
    }))
    this.setState($set('redirectToAddReview', true, this.state))
  }
}

export default ProductCard
