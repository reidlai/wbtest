import React, { Component } from 'react'
import { Redirect } from 'react-router'
import Header from 'grommet/components/Header'
import Title from 'grommet/components/Title'
import Button from 'grommet/components/Button'
import FormPrevious from 'grommet/components/icons/base/FormPrevious'
import {$set} from 'plow-js'

class AddReviewPageHeader extends Component {
  constructor(props) {
    super(props)
    this.state = $set('redirectToPrevious', false, props)
    this.goBack = this.goBack.bind(this)
  }

  render() {
    if(this.state.redirectToPrevious) {
      return (<Redirect to='/' />)
    }
    return (
      <Header>
        <Title>
          <Button icon={<FormPrevious onClick={this.goBack} />} />
          Add Review
        </Title>
      </Header>
    )
  }

  goBack() {
    this.setState($set('redirectToPrevious', true, this.state))
  }
}

export default AddReviewPageHeader
