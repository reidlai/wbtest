import React, { Component } from 'react'
import {connect} from 'react-redux'
import _ from 'lodash'
import moment from 'moment'
import Box from 'grommet/components/Box'
import AddReviewPageHeader from './AddReviewPageHeader.jsx'
import Form from 'grommet/components/Form'
import FormFields from 'grommet/components/FormFields'
import Footer from 'grommet/components/Footer'
import Button from 'grommet/components/Button'
import Label from 'grommet/components/Label'
import Select from 'grommet/components/Select'
import Toast from 'grommet/components/Toast'
import TextInput from 'grommet/components/TextInput'
import {$set, $all} from 'plow-js'
import {Redirect} from 'react-router-dom'

import reduxActions from '../redux-actions'

export class AddReviewPage extends Component {
  constructor(props) {
    super(props)
    this.state = props
    this.addReview = this.addReview.bind(this)
    this.onProductSelectChanged = this.onProductSelectChanged.bind(this)
    this.onRatingSelectChanged = this.onRatingSelectChanged.bind(this)
    this.onEmailInputChanged = this.onEmailInputChanged.bind(this)
    this.onRevieCommentChanged = this.onRevieCommentChanged.bind(this)
    this.validateEmail = this.validateEmail.bind(this)
  }

  handleStateChange() {
    let reduxState = store.getState()
    this.setState(_.defaultsDeep(mapStateToProps(reduxState), this.state))
  }

  componentDidMount() {
    this.unsubscribe = store.subscribe(this.handleStateChange.bind(this))
  }

  componentWillUnmount() {
    this.unsubscribe()
  }

  render() {

    let productComponent = null
    if(this.state.match.params.productId) {
      productComponent = <Label>{this.state.selectedProductOption.label}</Label>
    } else {
      productComponent = <Select options={this.state.productOptions}
      placeHolder='Product' value={this.state.selectedProductOption}
      onChange={this.onProductSelectChanged} />
    }

    if(this.state.redirectToPrevious) {
      return (<Redirect to='/' />)
    }

    let badRatingOption = null
    if(this.state.badRatingOption) {
      badRatingOption = badRatingOptionToast
    }

    let badProductOption = null
    if(this.state.badProductOption) {
      badProductOption = badProductOptionToast
    }

    let badEmailInput = null
    if(this.state.badEmailInput) {
      badEmailInput = badEmailInputToast
    }

    let badReviewComment = null
    if(this.state.badReviewComment) {
      badReviewComment = badReviewCommentToast
    }

    let addReviewFailed = null
    if(this.state.addReviewFailed) {
      addReviewFailed = addReviewFailedToast
    }

    return (
      <Box flex={true} full={true}>
        {badRatingOption}
        {badProductOption}
        {badEmailInput}
        {badReviewComment}
        {addReviewFailed}
        <AddReviewPageHeader />
        <Box flex={true} full={true} direction='column' justify='center' align='center' >
          <Form>
            <FormFields>
              <Box colorIndex='light-1' pad='small'>
                {productComponent}
              </Box>
              <Box colorIndex='light-1' pad='small'>
                <Select options={this.state.ratingOptions} placeHolder='Rating'
                value={this.state.selectedRatingOption}
                onChange={this.onRatingSelectChanged} />
              </Box>
              <Box colorIndex='light-1' pad='small'>
                <TextInput id='emailInput' name='emailInput' value={this.state.email} onDOMChange={this.onEmailInputChanged} />
              </Box>
              <Box colorIndex='light-1' pad='small'>
                <textarea value={this.state.reviewComment}
                onChange={this.onRevieCommentChanged}
                style={{backgroundColor: '#FFFFFF', width: '100%',
                minHeight: '5em'}} />
              </Box>
            </FormFields>
            <Footer pad={{"vertical": "medium"}}>
              <Button label='Submit' type='submit' primary={true}
              onClick={this.addReview}/>
            </Footer>
          </Form>
        </Box>
      </Box>
    )
  }

  addReview(event) {
    event.preventDefault()

    if (!this.state.selectedProductOption || !this.state.selectedProductOption.value) {
      store.dispatch(reduxActions.setBadProductOptionAction())
    } else if (!this.state.selectedRatingOption || !_.inRange(this.state.selectedRatingOption,0,10)) {
      store.dispatch(reduxActions.setBadRatingOptionAction())
    } else if (!this.state.email.length > 0 || !this.validateEmail()) {
      store.dispatch(reduxActions.setBadEmailInputAction())
    } else if (!this.state.reviewComment.length > 0) {
      store.dispatch(reduxActions.setBadReviewCommentAction())
    } else {
      store.dispatch(reduxActions.addReviewEvent({
        reviewRating: this.state.selectedRatingOption,
        reviewComment: this.state.reviewComment,
        creationTimestamp: moment().valueOf(),
        createdBy: this.state.userId,
        productId: this.state.selectedProductOption.value
      }))
    }
  }

  onProductSelectChanged(options) {
    store.dispatch(reduxActions.setSelectedProductAction(options.option))
  }

  onRatingSelectChanged(options) {
    store.dispatch(reduxActions.setSelectedRatingAction(options.option))
  }

  onEmailInputChanged(event) {
    store.dispatch(reduxActions.setEmailInputAction(event.target.value))
  }

  onRevieCommentChanged(event) {
    store.dispatch(reduxActions.setReviewCommentAction(event.target.value))
  }

  validateEmail() {
    const returnValue = _.find(this.state.userProfileData, (item) => {
      return item.email === this.state.email
    })
    return returnValue
  }
}

const badRatingOptionToast = (
  <Toast status='critical'>
    Bad rating input
  </Toast>
)

const badProductOptionToast = (
  <Toast status='critical'>
    Bad product input
  </Toast>
)

const badEmailInputToast = (
  <Toast status='critical'>
    Bad email input and not a valid user
  </Toast>
)

const badReviewCommentToast = (
  <Toast status='critical'>
    Bad comment input
  </Toast>
)

const addReviewFailedToast = (
  <Toast status='critical'>
    Cannot add review
  </Toast>
)

const mapStateToProps = (state) => {
  return {
    ...state.productReducer,
    ...state.reviewReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {dispatch}
}

export default connect(mapStateToProps, mapDispatchToProps)(AddReviewPage)
