import React, { Component } from 'react'
import {connect} from 'react-redux'
import _ from 'lodash'
import Tiles from 'grommet/components/Tiles'
import Tile from 'grommet/components/Tile'
import Toast from 'grommet/components/Toast'
import ProductCard from './ProductCard.jsx'

import reduxActions from '../redux-actions'

export class ProductList extends Component {

  constructor(props) {
    super(props)
    this.state = props
  }

  handleStateChange() {
    let reduxState = store.getState()
    this.setState(_.defaultsDeep(mapStateToProps(reduxState), this.state))
  }

  componentDidMount() {
    store.dispatch(reduxActions.resetReviewReducerStateAction())
    this.unsubscribe = store.subscribe(this.handleStateChange.bind(this))
  }

  componentWillUnmount() {
    this.unsubscribe()
  }

  render() {
    let tileList = _.map(this.state.latestProductData, (item) => {
      return(
        <Tile key={`tile_${item.productId}`}>
          <ProductCard key={`card_${item.productId}`} product={item} />
        </Tile>
      )
    })
    let noProductFound = null
    if(!tileList || tileList.length === 0) {
      noProductFound = noProductFoundToast
    }
    return (
      <div>
        {noProductFound}
        <Tiles fill={true} flush={false} >
          {tileList}
        </Tiles>
      </div>
    )
  }
}

const noProductFoundToast = (
  <Toast status='warning'>
    No product was found.
  </Toast>
)

const mapStateToProps = (state) => {
  return {
    ...state.productReducer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {dispatch}
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductList)
