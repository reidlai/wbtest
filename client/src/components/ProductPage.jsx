import React, { Component } from 'react'
import Box from 'grommet/components/Box'
import ProductPageHeader from './ProductPageHeader.jsx'
import ProductList from './ProductList.jsx'

export default class ProductPage extends Component {
  constructor(props) {
    super(props)
    this.state = props
  }

  render() {
    return (
      <Box flex={true}>
        <ProductPageHeader />
        <ProductList />
      </Box>
    )
  }
}
