import reduxActions from '../redux-actions'
// import {$set} from 'plow-js'
import _ from 'lodash'

export const initialState = {
  filteredBrand: null,
  latestProductData: []
}

export const productReducer = (state = initialState, action) => {
  switch(action.type) {
    // When api call getting latest products finished successfully
    case reduxActions.GET_LATESTPRODUCTS_ACTION_SUCCESS:
      return _.defaultsDeep({latestProductData: action.result}, state)
    // When user choose brand from product list header
    case reduxActions.SET_BRAND_FILTER_ACTION:
      return _.defaultsDeep({filteredBrand: action.brandOption}, state)
    default:
      return state
  }
}
