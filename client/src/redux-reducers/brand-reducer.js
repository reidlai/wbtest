import reduxActions from '../redux-actions'
import _ from 'lodash'

export const initialState = {
  brandData: []
}

export const brandReducer = (state = initialState, action) => {
  switch(action.type) {
    // When api call getting brand list finished successfully
    case reduxActions.GET_BRANDLIST_ACTION_SUCCESS:
      return _.defaultsDeep({brandData: action.result})
    default:
      return state
  }
}
