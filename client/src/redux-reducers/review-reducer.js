import reduxActions from '../redux-actions'
// import {$set,$all} from 'plow-js'
import _ from 'lodash'

export const initialState = {
  userProfileData: null,
  ratingOptions: [0,1,2,3,4,5,6,7,9,10],
  productOptions: [],
  selectedProductOption: null,
  selectedRatingOption: null,
  reviewComment: '',
  email: '',
  userId: 2,
  redirectToPrevious: false,
  addReviewFailed: false,
  badRatingOption: false,
  badProducctOption: false,
  badEmailInput: false,
  badReviewComment: false
}

export const reviewReducer = (state = initialState, action) => {
  switch(action.type) {
    // When api call getting full product list for product select options
    // finished successfully
    case reduxActions.GET_PRODUCTLIST_ACTION_SUCCESS:
      const productOptions = _.map(action.result, (item) => {
        return {label: item.productName, value: item.productId}
      })
      return _.defaultsDeep({productOptions}, state)
    // When user select product in add review page
    case reduxActions.SET_SELECTED_PRODUCT_ACTION:
      return _.defaultsDeep({selectedProductOption: action.productOption},
        state)
    // When user select rating option in add review page
    case reduxActions.SET_SELECTED_RATING_ACTION:
      return _.defaultsDeep({selectedRatingOption: action.ratingOption}, state)
    // When user enter email in add review page
    case reduxActions.SET_EMAIL_INPUT_ACTION:
      return _.defaultsDeep({email: action.value}, state)
    // When user enter review comment in add review page
    case reduxActions.SET_REVIEW_COMMENT_ACTION:
      return _.defaultsDeep({reviewComment: action.value}, state)
    // Show bad rating toast
    case reduxActions.SET_BAD_RATING_OPTION_ACTION:
      return _.defaultsDeep({badRatingOption: true}, state)
    // Show bad product toast
    case reduxActions.SET_BAD_PRODUCT_OPTION_ACTION:
      return _.defaultsDeep({badProductOption: true}, state)
    // show bad email toast
    case reduxActions.SET_BAD_EMAIL_INPUT_ACTION:
      return _.defaultsDeep({badEmailInput: true}, state)
    // show bad review comment toast
    case reduxActions.SET_BAD_REVIEW_COMMENT_ACTION:
      return _.defaultsDeep({badReviewComment: true}, state)
    // Successfully call api to create new review
    case reduxActions.ADD_REVIEW_ACTION_SUCCESS:
      return _.defaultsDeep({redirectToPrevious: true}, state)
    // Failed to create new review
    case reduxActions.ADD_REVIEW_ACTION_FAILED:
      return _.defaultsDeep({addReviewFailed: true}, state)
    // Reset review reducer state
    case reduxActions.RESET_REVIEW_REDUCER_STATE_ACTION:
      return initialState
    // When api call getting user profile finished successfully
    case reduxActions.GET_USERPROFILELIST_ACTION_SUCCESS:
      return _.defaultsDeep({userProfileData: action.result}, state)
    default:
      return state
  }
}
