import { combineReducers } from 'redux'
import { brandReducer } from './brand-reducer'
import { productReducer } from './product-reducer'
import { reviewReducer } from './review-reducer'

const reducers = combineReducers({
  brandReducer,
  productReducer,
  reviewReducer
})

export default reducers
