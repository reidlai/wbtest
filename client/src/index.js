import React from 'react'
import ReactDOM from 'react-dom'
import injectTapEventPlugin from 'react-tap-event-plugin'
import {createStore, applyMiddleware} from 'redux'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'
import reduxLogger from 'redux-logger'
import App from './components/App.jsx'

import './stylesheets/index.scss'

import reduxActions from './redux-actions'
import reduxReducers from './redux-reducers'

const StoreProvider = Provider

// create redux store
let store = createStore(reduxReducers, applyMiddleware(thunk, reduxLogger))

// set store and dispatch as global variables
window.store = store
window.dispatch = store.dispatch

// optional, capture tap event in mobile
injectTapEventPlugin()

// Main React Component
ReactDOM.render(
  <StoreProvider store={store}>
    <App />
  </StoreProvider>,
  document.getElementById('root')
)

// Dispatch async actions to call api to retrive data
dispatch(reduxActions.getBrandListEvent())
dispatch(reduxActions.getProductListEvent())
dispatch(reduxActions.getUserProfileListEvent())
dispatch(reduxActions.getLatestProductsEvent())

/**
 * Test for fetch outside redux
 **/
const fetchTestOutsideReduxStore = (brandId) => {
  let url = '/api/Products/getLatestProducts'
  if(brandId) {
    url = url + "?brandId=" + brandId
  }
  const fetchOptions = {
    method: 'GET',
    headers: {
      'Accept': 'application/json'
    }
  }
  fetch(url, fetchOptions)
  .then((response) => {
    if(response.status < 200 || response.status > 299) {
      throw new Error("Bad response from server");
    }
    return response.json()
  })
  .then((json) => {
    console.log("fetchTestOutsideReduxStore", "brandId", brandId, json.data)
  })
  .catch((err)=>{
    store.dispatch(getLatestProductsActionFailed(err))
  })
}

fetchTestOutsideReduxStore(null)
fetchTestOutsideReduxStore(1)
fetchTestOutsideReduxStore(2)
fetchTestOutsideReduxStore(3)
