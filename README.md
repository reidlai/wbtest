# Walton Brown Platform Test by Reid Lai

## Summary
This project contains all source code for WB Test requirement.

At the beginning, I received user stories (reuirement) including acceptance critera so I started to write Gherkin features and use CucumberJS to run all DB and API tests.  Due to limited timeframe, I skip Cucumber UI tests and only use TDD for React development.

## Framework and Libraries Justification
Due to limited time, I used the following framework for backend development

### Docker and Docker Compose
Docker and Docker Compose can provide microservcie container and streamline local development environment up to production environment.  All pre-built image can be pulled from docker hub, e.g. MySQL and NodeJS.

### Loopback.io (IBM Strongloop) 
Loopback.io is the core api platform in this project.  It can base on model definition to render API.  And developer can also generate model and api from swagger document which is now de facto standard for API documentation.  

Another advange to use loopback.io is its integration capability. Loopback.io can support many RDMBMS and NoSQL engine with prebuilt connectors which can be found in GitHub.

### MySQL
Well-known free open source RDBMS and availabe in Docker Hub.

### React, Redux and React-Thunk
Component based web app framewrok to ease front-end develoment with Redux One-way MVVM.  React-thunk is providing aysnc action feature as redux middleware.

### Grommet
I am not skillful at pixel-level development so chose Grommet to speed up front-end devleopment time.  Although this library is not mature but it's good enough for this test.

### ESLint
ESLint can check standard coding style and bugs against source code.

### NSP
Vulnerabilities checking

### Babel and Webpack
Babel is used for ES6/ES7/React support while webpack can bundle all required front-end assets into single bundle JS file to reduce network resource.

## Known Issues
 - Inside client web app, I found FETCH api returned response object with mixup of old data and new data.  API works in both loopback explorer (**http://\<host>:3000/explorer**) and UAT test.  
 - Due to data mixup at Fetch API, Toast message cannot be shown for no product found.
 - Email input has been restricted to register user id.  New user can be added throught loopback explorer or use your api call POST /api/UserProfiles.
 
> After drilling down this problem, I found that FETCH api works good outside redux store.  When running fetch and dispatch redux action together in same script, problem will occur.  I have add the following code fragment in client/src/index.js, it show correct information in browser developer console and prove my finding.  If have more time, I will find a solution.

>###Fetch in Redux Store
>
> ![Fetch in redux store](docs/Screen Shot 2017-05-22 at 6.36.51 PM.png)
> 
> ###Fetch outside Redux Store 
> 
>![Fetch in redux store](docs/Screen Shot 2017-05-22 at 6.38.58 PM.png)

```Javascript
const fetchTestOutsideReduxStore = (brandId) => {
  let url = '/api/Products/getLatestProducts'
  if(brandId) {
    url = url + "?brandId=" + brandId
  }
  const fetchOptions = {
    method: 'GET',
    headers: {
      'Accept': 'application/json'
    }
  }
  fetch(url, fetchOptions)
  .then((response) => {
    if(response.status < 200 || response.status > 299) {
      throw new Error("Bad response from server");
    }
    return response.json()
  })
  .then((json) => {
    console.log("fetchTestOutsideReduxStore", "brandId", brandId, json.data)
  })
  .catch((err)=>{
    store.dispatch(getLatestProductsActionFailed(err))
  })
}

fetchTestOutsideReduxStore(null)
fetchTestOutsideReduxStore(1)
fetchTestOutsideReduxStore(2)
fetchTestOutsideReduxStore(3)
```


## Prerequite
I am assumed that this test will be run in Mac OSX and the follow prerequisite should be ready

 - Docker for Mac version 17.03.1-ce (API version 1.27) or above
 - Docker Compose version 1.11.2 or above
 - NodeJS version 7.10.0 and npm version 4.2.0 
 - Git version 2.9.0 or above
 
## Source reposiotry
All source code can be found at [My BitBucket repo](https://bitbucket.org/reidlai/wbtest.git).  Please use git to clone the repository as follow:

```
git clone https://bitbucket.org/reidlai/wbtest.git
```

## Installation
Once cloned, go to project reposiotry and install npm modules:

```
cd <repo>
npm install
```

## Quck start
For quick start, I recommend to use Docker Compose to bring up database and api platform as follow:

```
cd <repo>
docker-compose build
docker-compose up -d
docker ps
```

After you run the above command, there should be two docker container running, one is <repo>_mysql_1 and another one is <repo>_loopback_1.

And now you have to prepare MySQL database schema.  Please run the following commands:

```
docker exec -it <repo>_mysql_1 mysql /bin/bash
#>mysql --user=wbtest --password < /tmp/mysql/wbtest.sql
exit // exit mysql client
exit // exit bash shell
```

## Testing Data
Now you need testing data for UI testing.  All testing data is prepared by BDD script.  Please refere section Automatic UAT (BDD)

## Automatic UAT (BDD)
I started all backend development based on Gherkin and Cucumber to test all database schema and API.  The following command can run for automatic UAT:

```
cd <repo>
npm run e2e
```

All features file can found at <repo>/features, step definition files at <repo>/step_definitions, and hooks at repo/hooks.  If database server and api server url change, you can set environment varialbes for this requirement.  All environment variables can be checked out at hooks.

## Loopback Explorer
Before UI test, you may want to check all api.  Loopback explorer is available at 
http://\<host>:3000/explorer.  This is Swagger UI oriented explorer to list all available api.


## Web App Access
Web App can start at http://\<host>:3000.

## Continous Integration Options

### Linting
Due to frequent typo of myself, I use ESlint to help me checking all typo, bugs, and coding styles.  I didn't implement CI server or git pre-commit hook to automate linting process but can be implemented in future.  The follwoing command can run eslint

```
cd <repo>
npm run lint
```

### Vulnerabilities checking
This is optional.  NSP can help developers to avoid npm modules vulnerabilies.  The following command can run nsp

```
cd <repo>
npm run nsp
```

### Front-end deployment
If you like to rebuild front-end web app, you can.  Babel and Webpack is used to bundle all javasript and stylesheets into single file for faster loading.  The following command can run webpack and babel compiler setting is also included:

```
cd <repo>
npm run webpack
```




