Feature:
  As a customer, I want to call a web service to create a review so that I can
  give feedback on a product.

  Background:
    Given table UserProfile in database wbtest has the following rows:
      | userId | userType | userName | email | dateOfBirth |
      | 1      | Merchant | merchant1 | merchant1@example.com | 2000-01-01 |
      | 2      | Customer | customer1 | customer1@example.com | 2000-01-01 |
    And table Brand in database wbtest has the following rows:
      | brandId | brandName | brandDescription |
      | 1       | Brand 1   | Brand 1 Desc     |
      | 2       | Brand 2   | Brand 2 Desc     |
    And table Product in database wbtest has the following rows:
      | productId | productName | productDescription | unitPrice | color    | creationTimestamp | availabilityStatus | brandId |
      | 1         | Product 1   | Product 1 Desc     | 1.00      | Color 1  | 2016-01-01        | Archived           | 1       |
      | 2         | Product 2   | Product 2 Desc     | 2.00      | Color 2  | 2016-02-01        | Out of Stock       | 2       |
      | 3         | Product 3   | Product 3 Desc     | 3.00      | Color 3  | 2016-03-01        | Out of Stock       | 2       |
      | 4         | Product 4   | Product 4 Desc     | 4.00      | Color 4  | 2016-04-01        | Out of Stock       | 2       |
      | 5         | Product 5   | Product 5 Desc     | 5.00      | Color 5  | 2016-05-01        | In Stock           | 2       |
      | 6         | Product 6   | Product 6 Desc     | 6.00      | Color 6  | 2016-06-01        | In Stock           | 2       |
      | 7         | Product 7   | Product 7 Desc     | 7.00      | Color 7  | 2016-07-01        | In Stock           | 2       |
      | 8         | Product 8   | Product 8 Desc     | 8.00      | Color 8  | 2016-08-01        | In Stock           | 2       |
      | 9         | Product 9   | Product 9 Desc     | 9.00      | Color 9  | 2016-09-01        | In Stock           | 2       |
      | 10        | Product 10  | Product 10 Desc    | 10.00     | Color 10 | 2016-10-01        | In Stock           | 2       |
      | 11        | Product 11  | Product 11 Desc    | 11.00     | Color 11 | 2016-11-01        | In Stock           | 2       |
      | 12        | Product 12  | Product 12 Desc    | 12.00     | Color 12 | 2016-12-01        | In Stock           | 2       |
    And table Review in database wbtest has the following rows:
      | reviewId | reviewRating | reviewComment | productId | createdBy | creationTimestamp |
      | 1        | 5            | Comment 1     | 1         | 2         | 2015-12-31        |
      | 2        | 10           | Comment 2     | 12        | 2         | 2016-12-31        |

  @api
  Scenario: accept parameter to create review
    Given I set the following JSON data in body:
      """
      {
        "reviewRating": 6,
        "reviewComment": "Comment 3",
        "productId": 12,
        "createdBy": 2,
        "creationTimestamp": "2016-12-31"
      }
      """
    When I call api POST /api/Reviews
    # loopback default post return code is 200
    Then response code should be 200
    And new review should have been created

  @api
  Scenario: rating range validation
    Given I set the following JSON data in body:
      """
      {
        "reviewRating": 12,
        "reviewComment": "Comment 3",
        "productId": 12,
        "createdBy": 2,
        "creationTimestamp": "2016-12-31"
      }
      """
    When I call api POST /api/Reviews
    Then response code should be 422
    And new review should have not been created

  @api
  Scenario: invalid user id validation
    Given I set the following JSON data in body:
      """
      {
        "reviewRating": 6,
        "reviewComment": "Comment 3",
        "productId": 12,
        "createdBy": 6,
        "creationTimestamp": "2016-12-31"
      }
      """
    When I call api POST /api/Reviews
    Then response code should be 500
    And new review should have not been created
