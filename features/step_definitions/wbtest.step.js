var {defineSupportCode} = require('cucumber');
var fetch = require('isomorphic-fetch');
var mysql = require('mysql');
var chai = require('chai');
var expect = chai.expect;
var format = require('string-template');
var _ = require('lodash');

/* eslint no-unused-vars: ["error", { "varsIgnorePattern": "^test" }] */
defineSupportCode(function({Given, When, Then}) {
  Given(/^the database server is available$/, function() {
    this.connection = mysql.createConnection({
      host: this.mysqlHost,
      port: this.mysqlPort,
      user: this.mysqlUser,
      password: this.mysqlPassword,
      database: this.mysqlDatabase,
    });
    var testResult = expect(this.connection).to.not.be.null;
  });

  Given(/^table (.*) in database (.*) has the following rows:$/,
  function(tableName, databaseName, table, callback) {
    var samplerows = table.hashes();
    var sql1 = 'TRUNCATE TABLE {0}.{1}';
    this.connection = mysql.createConnection({
      host: this.mysqlHost,
      port: this.mysqlPort,
      user: this.mysqlUser,
      password: this.mysqlPassword,
      database: this.mysqlDatabase,
    });
    var connection = this.connection;
    connection.query('SET FOREIGN_KEY_CHECKS = 0', function(err) {
      if (!err) {
        connection.query(format(sql1, [databaseName, tableName]),
        function(err) {
          var testResult = expect(err).to.be.null;
          for (var i = 0; i < samplerows.length; i++) {
            var sql2 = 'INSERT INTO ' + databaseName + '.' + tableName;
            var col2 = [];
            var val2 = [];
            var bindString = '';
            _.forEach(samplerows[i], function(value, key) {
              col2.push(key);
              val2.push(value);
              if (bindString.length > 0) {
                bindString = bindString + ',';
              }
              bindString = bindString + '?';
            });
            sql2 = sql2 + ' (' + _.toString(col2) + ') VALUES (' + bindString +
            ')';
            connection.query(sql2, val2, function(err) {
              var testResult = expect(err).to.be.null;
              connection.query('SET FOREIGN_KEY_CHECKS = 0');
              callback(err);
            });
          }
        });
      }
    });
  });

  Given(/^I set the following JSON data in body:$/, function(docString) {
    this.stepContext.bodyString = _.replace(docString, '"""', '');
    expect(this.stepContext.bodyString).to.be.equal(docString);
  });

  When(/^connect with user id (.*) and password is (.*)$/,
  function(userName, password, callback) {
    this.connection.connect(function(err) {
      var testResult = expect(err).to.be.null;
      callback(err);
    });
  });

  When(/^I call api GET (.*)$/, function(uriString, callback) {
    var fetchOptions = {method: 'GET'};
    var uri = this.apiProtocol + '://' + this.apiHost + (this.apiPort ? ':' +
    this.apiPort : '') + uriString;
    var stepContext = this.stepContext;
    fetch(uri, fetchOptions)
    .then(function(response) {
      stepContext.statusCode = response.status.toString();
      return response.json();
    })
    .then(function(json) {
      stepContext.result = json;
      callback();
    });
  });

  When(/^I call api POST (.*)$/, function(uriString, callback) {
    var fetchOptions = {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'POST',
      body: this.stepContext.bodyString,
    };
    var uri = this.apiProtocol + '://' + this.apiHost + (this.apiPort ? ':' +
    this.apiPort : '') + uriString;
    var stepContext = this.stepContext;
    fetch(uri, fetchOptions)
    .then(function(response) {
      stepContext.statusCode = response.status.toString();
      return response.json();
    })
    .then(function(json) {
      stepContext.result = json;
      callback();
    });
  });

  Then(/^table (.*) should exist in the database (.*)$/,
  function(tableName, databaseName, callback) {
    var sql = 'SELECT 1 FROM information_schema.TABLES WHERE ' +
    'TABLE_SCHEMA=\'{0}\' AND TABLE_NAME=\'{1}\'';
    this.connection.query(format(sql, [databaseName, tableName]),
    function(err, result) {
      var testResult = expect(err).to.be.null;
      testResult = expect(result.length).to.be.equal(1);
      callback(err);
    });
  });

  Then(/^table (.*) should have the following fields in database (.*):$/,
  function(tableName, databaseName, table, callback) {
    var sql = 'SELECT 1 FROM information_schema.COLUMNS WHERE ' +
    'TABLE_SCHEMA=\'{0}\' AND TABLE_NAME=\'{1}\' AND COLUMN_NAME=\'{2}\'';
    var columns = table.hashes();
    for (var i = 0; i < columns.length; i++) {
      var bindSql = format(sql, [databaseName, tableName, columns[i].columnName]
      );
      this.connection.query(bindSql, function(err, result) {
        var testResult = expect(err).to.be.null;
        testResult = expect(result.length).to.be.equal(1);
        callback(err);
      });
    }
  });

  Then(/^response code should be (.*)$/, function(statusCode) {
    expect(this.stepContext.statusCode).to.be.equal(statusCode);
  });

  Then(/^the result should be (.*) with length less than or equal to (.*)$/,
  function(dataType, maxLengthString) {
    var maxLength = parseInt(maxLengthString) + 1;
    expect(this.stepContext.result.data.length).to.be.above(-1);
    expect(this.stepContext.result.data.length).to.be.below(maxLength);
  });

  Then(/^the result should be empty array$/, function() {
    expect(this.stepContext.result.data.length).to.be.equal(0);
  });

  Then(/^new review should have been created$/, function() {
    expect(this.stepContext.result.reviewId).to.be.above(0);
  });

  Then(/^new review should have not been created$/, function() {
    expect(this.stepContext.result.reviewId).to.be.NaN; // eslint-disable-line no-unused-expressions
  });
});
