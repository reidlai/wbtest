Feature: Database Schema
  As a developer, I need to store Product, Brand, and Review table in database
  so that I can perform CRUD through api platform

  @database
  Scenario: Validate Brand schema
    Given the database server is available
    When connect with user id wbtest and password is wbtest
    Then table Brand should exist in the database wbtest
    And table Brand should have the following fields in database wbtest:
      | columnName       |
      | brandId          |
      | brandName        |
      | brandDescription |

  @database
  Scenario: Validate Product schema
    Given the database server is available
      When connect with user id wbtest and password is wbtest
      Then table Product should exist in the database wbtest
      And table Product should have the following fields in database wbtest:
        | columnName         |
        | productId          |
        | productName        |
        | productDescription |
        | unitPrice          |
        | color              |
        | creationTimestamp  |
        | availabilityStatus |

  @database
  Scenario: Validate Review schema
    Given the database server is available
      When connect with user id wbtest and password is wbtest
      Then table Review should exist in the database wbtest
      And table Review should have the following fields in database wbtest:
        | columnName    |
        | reviewId      |
        | reviewRating  |
        | reviewComment |

  @database
  Scenario: Validate UserProfile schema
    Given the database server is available
      When connect with user id wbtest and password is wbtest
      Then table UserProfile should exist in the database wbtest
      And table UserProfile should have the following fields in database wbtest:
        | columnName  |
        | userId      |
        | userType    |
        | userName    |
        | email       |
        | dateOfBirth |
