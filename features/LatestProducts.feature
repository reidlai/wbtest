Feature: get top 10 latest products
  As a developer, I want to use RESTful/JSON client to get latest products so
  that the result can be display in web app.
  Acceptance Criteria:
    - Returns top 10 newest products;
    - For each product, needs to include the Product ID, Name, Description, Brand Name and the most recent Review for this product (including the User Name and Review Summary);
    - Accepts an optional Brand ID parameter to filter the results. If a Brand ID provided, returns only products for that brand.

  Background:
    Given table UserProfile in database wbtest has the following rows:
      | userId | userType | userName | email | dateOfBirth |
      | 1      | Merchant | merchant1 | merchant1@example.com | 2000-01-01 |
      | 2      | Customer | customer1 | customer1@example.com | 2000-01-01 |
    And table Brand in database wbtest has the following rows:
      | brandId | brandName | brandDescription |
      | 1       | Brand 1   | Brand 1 Desc     |
      | 2       | Brand 2   | Brand 2 Desc     |
    And table Product in database wbtest has the following rows:
      | productId | productName | productDescription | unitPrice | color    | creationTimestamp | availabilityStatus | brandId |
      | 1         | Product 1   | Product 1 Desc     | 1.00      | Color 1  | 2016-01-01        | Archived           | 1       |
      | 2         | Product 2   | Product 2 Desc     | 2.00      | Color 2  | 2016-02-01        | Out of Stock       | 2       |
      | 3         | Product 3   | Product 3 Desc     | 3.00      | Color 3  | 2016-03-01        | Out of Stock       | 2       |
      | 4         | Product 4   | Product 4 Desc     | 4.00      | Color 4  | 2016-04-01        | Out of Stock       | 2       |
      | 5         | Product 5   | Product 5 Desc     | 5.00      | Color 5  | 2016-05-01        | In Stock           | 2       |
      | 6         | Product 6   | Product 6 Desc     | 6.00      | Color 6  | 2016-06-01        | In Stock           | 2       |
      | 7         | Product 7   | Product 7 Desc     | 7.00      | Color 7  | 2016-07-01        | In Stock           | 2       |
      | 8         | Product 8   | Product 8 Desc     | 8.00      | Color 8  | 2016-08-01        | In Stock           | 2       |
      | 9         | Product 9   | Product 9 Desc     | 9.00      | Color 9  | 2016-09-01        | In Stock           | 2       |
      | 10        | Product 10  | Product 10 Desc    | 10.00     | Color 10 | 2016-10-01        | In Stock           | 2       |
      | 11        | Product 11  | Product 11 Desc    | 11.00     | Color 11 | 2016-11-01        | In Stock           | 2       |
      | 12        | Product 12  | Product 12 Desc    | 12.00     | Color 12 | 2016-12-01        | In Stock           | 2       |
    And table Review in database wbtest has the following rows:
      | reviewId | reviewRating | reviewComment | productId | createdBy | creationTimestamp |
      | 1        | 5            | Comment 1     | 1         | 2         | 2015-12-31        |
      | 2        | 10           | Comment 2     | 12        | 2         | 2016-12-31        |

  @api
  Scenario: return top 10 newest products
    When I call api GET /api/Products/getLatestProducts
    Then response code should be 200
    And the result should be array with length less than or equal to 10


  @api
  Scenario: filter by brand id
    When I call api GET /api/Products/getLatestProducts?brandId=1
    Then response code should be 200
    And the result should be array with length less than or equal to 1

  @api
  Scenario: filter by invalid brand id
    When I call api GET /api/Products/getLatestProducts?brandId=4
    Then response code should be 200
    And the result should be empty array
