var {defineSupportCode} = require('cucumber');
var nconf = require('nconf');
var apickli = require('apickli');

nconf
  .env()
  .defaults({
    MYSQL_HOST: 'localhost',
    MYSQL_PORT: '3306',
    MYSQL_DATABASE: 'wbtest',
    MYSQL_USER: 'wbtest',
    MYSQL_PASSWORD: 'wbtest',
    API_PROTOCOL: 'http',
    API_HOST: 'localhost',
    API_PORT: '3000',
    API_ROOT_URL: '/api',
  });

defineSupportCode(function({After, Before}) {
  Before(function() {
    this.mysqlHost = nconf.get('MYSQL_HOST');
    this.mysqlPort = nconf.get('MYSQL_PORT');
    this.mysqlDatbase = nconf.get('MYSQL_DATABASE');
    this.mysqlUser = nconf.get('MYSQL_USER');
    this.mysqlPassword = nconf.get('MYSQL_PASSWORD');
    this.stepContext = {};
  });

  Before('@api', function() {
    this.apiProtocol = nconf.get('API_PROTOCOL');
    this.apiHost = nconf.get('API_HOST');
    this.apiPort = nconf.get('API_PORT');
    this.apiRootUrl = nconf.get('API_ROOT_URL');
    this.apickli = new apickli.Apickli(this.apiProtocol, this.apiHost +
      this.apiPort ? (':' + this.apiPort) : '' + this.apiRootUrl);
  });

  After(function() {
    this.connection.end();
  });
});
