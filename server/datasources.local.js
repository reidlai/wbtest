var nconf = require('nconf');

nconf
  .env()
  .defaults({
    MYSQL_HOST: 'localhost',
    MYSQL_PORT: '3306',
    MYSQL_DATABASE: 'wbtest',
    MYSQL_USER: 'wbtest',
    MYSQL_PASSWORD: 'wbtest',
  });

module.exports = {
  'db': {
    'name': 'db',
    'connector': 'memory',
  },
  'mysql': {
    'name': 'mysql',
    'connector': 'mysql',
    'host': nconf.get('MYSQL_HOST'),
    'port': nconf.get('MYSQL_PORT'),
    'database': nconf.get('MYSQL_DATABASE'),
    'password': nconf.get('MYSQL_USER'),
    'user': nconf.get('MYSQL_PASSWORD'),
  },
};
