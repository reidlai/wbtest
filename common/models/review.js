module.exports = function(Review) {
  Review.validate('reviewRating', validateReviewRating,
  {message: 'Rating must be between 0 to 10'});
  function validateReviewRating(err) {
    if (this.reviewRating < 0 || this.reviewRating > 10) {
      err();
    }
  }
};
