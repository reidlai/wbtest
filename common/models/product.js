module.exports = function(Product) {
  Product.getLatestProducts = function(brandId = null, callback) {
    Product.app.dataSources['mysql'].connector.query(
    'call getLatestProducts(?)', [brandId], function(err, results) {
      callback(err, results[0]);
    });
  };

  Product.remoteMethod('getLatestProducts', {
    accepts: [
      {arg: 'brandId', type: 'number'},
    ],
    returns: {arg: 'data', type: 'array'},
    http: {verb: 'get'},
  });
};
