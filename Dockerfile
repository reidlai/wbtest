FROM node:7
RUN mkdir -p /usr/app/src
COPY package.json /usr/app/src/package.json
COPY node_modules /usr/app/src/node_modules
COPY server /usr/app/src/server
COPY common /usr/app/src/common
COPY client /usr/app/src/client
RUN ls /usr/app/src
WORKDIR /usr/app/src
CMD ["node ."]
