const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const CLIENT_DIR = path.resolve(__dirname, 'client');

const moduleTemplate = {
  loaders: [
    {
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        presets: ['es2015', 'stage-3', 'env'],
      },
    },
    {
      test: /\.jsx$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        presets: ['es2015', 'stage-3', 'env', 'react'],
      },
    },
    {
      test: /\.css$/,
      loader: 'style-loader!css-loader',
    },
    {
      test: /\.scss$/,
      loaders: [{
        loader: 'style-loader',
      }, {
        loader: 'css-loader',
      }, {
        loader: 'sass-loader',
        options: {
          outputStyle: 'compressed',
          includePaths: ['./node_modules'],
        },
      }],
    },
    {
      test: /\.less$/,
      loader: 'style-loader!css-loader!sass-loader',
    },
  ],
};

const config = [
  {
    entry: {
      'app': ['./client/src/index.js'],
    },
    output: {
      path: CLIENT_DIR + '/dist/javascripts',
      filename: '[name].bundle.js',
    },
    module: moduleTemplate,
    plugins: [
      new CopyWebpackPlugin([
        {
          from: CLIENT_DIR + '/src/index.html',
          to: CLIENT_DIR + '/dist/index.html',
        },
      ]),
    ],
  },
];

module.exports = config;
